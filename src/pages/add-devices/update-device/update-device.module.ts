import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UpdateDevicePage } from './update-device';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    UpdateDevicePage
  ],
  imports: [
    IonicPageModule.forChild(UpdateDevicePage),
    TranslateModule.forChild()
  ]
})
export class UpdateDevicePageModule {}
